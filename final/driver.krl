ruleset driver {
  meta {
    use module io.picolabs.subscription alias Subscriptions
    shares __testing
  }
  global {
    __testing = { "queries": [ { "name": "__testing" } ],
                  "events": [ ] }
  }
  
  rule initialization {
    select when wrangler ruleset_added where rids >< meta:rid
    if ent:driver_num.isnull() &&
      ent:orders_seen.isnull() then noop();
    fired {
      ent:driver_num := random:word();
      ent:orders_seen := ["abcde"];
    }
  }
  
  rule auto_accept {
    select when wrangler inbound_pending_subscription_added
    pre {
      attributes = event:attrs.klog("subcription:")
    }
    always {
      raise wrangler event "pending_subscription_approval"
        attributes attributes
    }
  }
  
  rule chosen {
    select when driver chosen
    pre {
      driver_num = event:attr("driver_num");
      order_num = event:attr("order_num");
      bid_time = event:attr("bid_time");
    }
    if ent:driver_num == driver_num then send_directive("I am driver "+driver_num+", chosen for order "+order_num)
    fired {
      ent:orders_seen := ent_orders_seen.any(function(x) {x != order_num});
      raise store event "delivery"
        attributes {
          "driver_num": driver_num,
          "order_num": order_num
        }
    }
  }
  
  rule request {
    select when driver request
    pre {
      order_num = event:attr("order_num");
      driver_num = ent:driver_num;
      time = random:number();
      seen_order = ent:orders_seen.any(function(x) {x == order_num})
    }
    if not seen_order then send_directive("driver "+driver_num+" sees request for order "+order_num)
    fired {
      ent:orders_seen := ent:orders_seen.append(order_num);
      raise store event "bid"
        attributes {
          "bid_time": time,
          "driver_num": driver_num,
          "order_num": order_num
        };
      //perpetuate gossip to any listening neighbor drivers
      //raise driver event "request"
        //attributes {
          //"order_num": order_num
        //}
    }
  }
  
  rule mischief_hat_lifted {
    select when mischief hat_lifted
    foreach Subscriptions:established("Tx_role","thing") setting (subscription)
      pre {
        thing_subs = subscription.klog("subs")
      }
      event:send(
        { "eci": subscription{"Tx"}, "eid": "hat-lifted",
          "domain": "mischief", "type": "hat_lifted" }
      )
  }
}
