ruleset store {
  meta {
    use module io.picolabs.subscription alias Subscriptions
    use module use_twilio
    shares __testing
  }
  global {
    __testing = { "queries": [ { "name": "__testing" } ],
                  "events": [ ] }
  }
  
  rule initialization {
    select when wrangler ruleset_added where rids >< meta:rid
    if ent:len.isnull() &&
      ent:bids.isnull() &&
      ent:send_from_phone.isnull() &&
      ent:phone.isnull() then noop();
    fired {
      ent:len := 1;
      ent:bids := {};
      ent:send_from_phone := "+16572228343";
      ent:phone := "+19499390011";
    }
  }
  
  rule auto_accept {
    select when wrangler inbound_pending_subscription_added
    pre {
      attributes = event:attrs.klog("subcription:")
    }
    always {
      raise wrangler event "pending_subscription_approval"
        attributes attributes
    }
  }
  
  rule order {
    select when store order
    pre {
      order_num = event:attr("order_num");
    }
    send_directive("store received order "+order_num)
    always {
      ent:bids{order_num} := [];
      raise driver event "request"
        attributes {
          "order_num": order_num
        }
    }
  }
  
  rule choose {
    select when store choose
    pre {
      order_num = event:attr("order_num");
      bids = ent:bids{order_num}.sort(function(a, b) {a[1] <=> b[1]});
      len = ent:len;
      bids_len = bids.length();
      max_bid = bids[bids_len-1]
    }
    if bids_len < len then send_directive("bids for order "+order_num+": "+bids.encode())
    notfired {
      ent:bids{order_num} := [];
      raise driver event "chosen"
        attributes {
          "driver_num": max_bid[0],
          "order_num": order_num,
          "bid_time": max_bid[1]
        }
    }
  }
  
  rule bid {
    select when store bid
    pre {
      bid_time = event:attr("bid_time");
      driver_num = event:attr("driver_num");
      order_num = event:attr("order_num");
    }
    send_directive("received bid for order "+order_num+
    " from driver "+driver_num+" for time "+bid_time)
    always {
      ent:bids{order_num} := ent:bids{order_num}.append([[driver_num, bid_time]]);
      raise store event "choose"
        attributes {
          "order_num": order_num
        }
    }
  }
  
  rule pickup {
    select when store pickup
    pre {
      driver_num = event:attr("driver_num");
      order_num = event:attr("order_num");
      phone = ent:phone;
      send_from = ent:send_from_phone;
      message = "order "+order_num+" picked up by driver "+driver_num
    }
    send_directive(message)
    always {
      raise test event "new_message"
        attributes { 
          "to": phone, 
          "from": send_from,
          "message": message
        }
    }
  }
  
  rule delivery {
    select when store delivery
    pre {
      driver_num = event:attr("driver_num");
      order_num = event:attr("order_num");
      phone = ent:phone;
      send_from = ent:send_from_phone;
      message = "order "+order_num+" delivered by driver "+driver_num
    }
    send_directive(message)
    always {
      ent:bids := ent:bids.delete(order_num);
      raise test event "new_message"
        attributes { 
          "to": phone, 
          "from": send_from,
          "message": message
        }
    }
  }
}
