ruleset use_twilio {
  meta {
    use module lesson_keys
    use module twilio
        with account_sid = keys:twilio{"account_sid"}
             auth_token =  keys:twilio{"auth_token"}
    shares __testing
  }
 
 global {
    __testing = { 
            "events": [ { "domain": "test", "type": "new_message",
                      "attrs": [ "to", "from", "message" ] },
                      { "domain": "test", "type": "messages",
                      "attrs": [ "to", "from", "message" ] } ]
        }
  }
 
  rule test_send_sms {
    select when test new_message
    every {
      send_directive("sms being sent")
      twilio:send_sms(event:attr("to"),
                      event:attr("from"),
                      event:attr("message")
                     )
    }
  }
  
  rule messages {
    select when test messages
    twilio:messages()
  }
    
}