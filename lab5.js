angular.module('lab5', [])
.controller('MainCtrl', [
  '$scope','$http',
  function($scope,$http){
    $scope.temperatures = [];
    $scope.violations = [];
    $scope.eci = "6hTWwHFJ2yEUEExqjXQCns";

    var gURL = '/sky/cloud/'+$scope.eci+'/temperature_store/current_temp';
    $scope.getAll = function() {
      return $http.get(gURL).success(function(data){
        angular.copy(data, $scope.current_temp);
      });
    };

    var gURL = '/sky/cloud/'+$scope.eci+'/temperature_store/temperatures';
    $scope.getAll = function() {
      return $http.get(gURL).success(function(data){
        angular.copy(data, $scope.temperatures);
      });
    };

    var gURL = '/sky/cloud/'+$scope.eci+'/temperature_store/threshold_violation';
    $scope.getAll = function() {
      return $http.get(gURL).success(function(data){
        angular.copy(data, $scope.violations);
      });
    };

    $scope.getAll();
  }
]);