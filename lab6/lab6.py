import http
import requests

print("Testing sensor management pico...\n")
URL = "http://localhost:8080/sky/"
parent_eci = "Tn5fpbaLi4Qdw6JEogctLA"
N = 5

# args is array of tuples with arg name and value
def event(eci, e_domain, e_type, args):
	URI = URL+'event/'+eci+'/none/'+e_domain+'/'+e_type
	for i in range(len(args)):
		if i == 0:
			URI += '?'
		else:
			URI += '&'
		URI += args[i][0]+'='+args[i][1]
	return requests.post(URI)

def query(eci, q_rid, q_name):
	URI = URL+'cloud/'+eci+'/'+q_rid+'/'+q_name
	return requests.get(URI).json()

# create multiple sensors
for i in range(N):
	event(parent_eci, "sensor", "new_sensor", [("name", "test"+str(i))])
print(N, 'sensors created')

# tests the sensor profile to ensure it's getting set reliably.
sensors = query(parent_eci, "manage_sensors", "sensors")
profiles = query(parent_eci, "manage_sensors", "check_profiles")
print('\nProfiles:\n', profiles)
# delete at least one sensor
event(parent_eci, "sensor", "unneeded_sensor", [('name', 'test0')])
print('\nsensor test0 deleted')
# update profile
event(sensors["test1"], "sensor", "profile_updated", [('name', "AAAAA"), ('location', "EEEEE"), ("threshold", "--------"), ('phone', '911 LOL')])
print('\nsensor test1 profile updated')
# reprint to verify
profiles = query(parent_eci, "manage_sensors", "check_profiles")
print('\nProfiles:\n', profiles)

temps = query(parent_eci, "manage_sensors", "check_temps")
sensors = query(parent_eci, "manage_sensors", "sensors")
print('\nTemps:\n', temps, '\n')

# You only have one Wovyn device, so you'll only have one sensor pico that is actually connected to a device. 
# Note: you'll have to reprogram the Wovyn sensor to send events to the new pico instead of the one you created manually.

# test the sensors by ensuring they respond correctly to new temperature events. 
for key in sensors:
	eci = sensors[key]
	event(eci, 'wovyn', 'test_heartbeat', [('genericThing', '69.69')])
	print('heartbeat temp sent to '+key)

temps = query(parent_eci, "manage_sensors", "check_temps")
print('\nTemps:\n', temps)


# clean up: delete leftover children
for i in range(N):
	event(parent_eci, "sensor", "unneeded_sensor", [("name", "test"+str(i))])
print(N-1, 'sensors deleted')
print('Testing complete.')
print()