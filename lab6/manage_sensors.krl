ruleset manage_sensors {
  meta {
    use module io.picolabs.wrangler alias wrangler

    shares __testing, 
          sensors, 
          check_temps,
          check_profiles
  }
  global {
    __testing = { "queries": [ { "name": "__testing" } ],
                  "events": [ ] }
                  
  sensors = function() {
    ent:sensors;
  }
  
  check_temps = function() {
    ent:sensors.map(function(v,k) {
      wrangler:skyQuery(v, "temperature_store", "temperatures");
    })
  }
  
  check_profiles = function() {
    ent:sensors.map(function(v,k) {
      wrangler:skyQuery(v, "sensor_profile", "get_info")
    })
  }
}

  rule initialization {
    select when wrangler ruleset_added where rids >< meta:rid
    if ent:sensors.isnull() &&
       ent:threshold.isnull() &&
       ent:default_location.isnull() &&
       ent:default_phone.isnull() then noop();
    fired {
      ent:sensors := {};
      ent:threshold := 80;
      ent:default_location := "provo";
      ent:default_phone := "+9499390011";
    }
  }
  
  rule new_sensor {
    select when sensor new_sensor
    pre {
      pico_name = event:attr("name")
      name_exists = ent:sensors >< pico_name
    }
    if name_exists then
      send_directive("duplicate name: pico not created")
    notfired {
      raise wrangler event "child_creation"
        attributes { "name": pico_name,
                    "rids": ["temperature_store", 
                            "wovyn_base", 
                            "sensor_profile"]}
    }
  }
  
  rule store_new_sensor {
    select when wrangler child_initialized
    pre {
      name = event:attr("rs_attrs"){"name"};
      eci = event:attr("eci");
      location = ent:default_location;
      threshold = ent:threshold;
      phone = ent:default_phone;
    }
    event:send(
      { "eci": eci, "eid": "sensor_profile",
        "domain": "sensor", "type": "profile_updated",
        "attrs": { "name": name, 
                  "location": location,
                  "threshold": threshold,
                  "phone": phone } } )
    always {
      ent:sensors := ent:sensors.put([name], eci)
    }
  }
  
  rule unneeded_sensor {
    select when sensor unneeded_sensor
    pre {
      pico_name = event:attr("name")
      name_exists = ent:sensors >< pico_name
    }
    if name_exists then
      send_directive("pico "+pico_name+" deleted")
    fired {
      raise wrangler event "child_deletion"
        attributes {"name": pico_name};
      ent:sensors := ent:sensors.delete(pico_name)
    }
  }
}
