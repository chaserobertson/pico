ruleset wovyn_base {
  meta {
    use module use_twilio
    use module sensor_profile
    shares __testing, threshold, phone
  }
  global {
    __testing = { "queries": [ { "name": "__testing" } ],
                  "events": [ ] };
                  
  threshold = function() {
    sensor_profile:get_threshold().as("Number");
  }
  phone = function() {
    "+"+sensor_profile:get_phone();
  }
  }
  
  rule intialization {
    select when wrangler ruleset_added where rids >< meta:rid
    if ent:send_from_phone.isnull() then noop();
    fired {
      ent:send_from_phone := "+16572228343";
    }
  }
  
  rule process_heartbeat {
    select when wovyn heartbeat
    pre {
      genericThing = event:attr("genericThing").defaultsTo(0);
      timestamp = time:now();
    }
    if genericThing then every {
      send_directive("temperature received")
    }
    fired {
      raise wovyn event "new_temperature_reading"
        attributes { 
          "temperature": genericThing["data"]["temperature"][0]["temperatureF"],
          "timestamp": timestamp }
    }
  }
  
  rule process_test_heartbeat {
    select when wovyn test_heartbeat
    pre {
      genericThing = event:attr("genericThing").defaultsTo(0);
      timestamp = time:now();
    }
    if genericThing then every {
      send_directive("temperature received")
    }
    fired {
      raise wovyn event "new_temperature_reading"
        attributes { 
          "temperature": genericThing.as("Number"),
          "timestamp": timestamp }
    }
  }
  
  rule find_high_temps {
    select when wovyn new_temperature_reading
    if event:attr("temperature") > sensor_profile:get_threshold().as("Number") then every {
      send_directive("temperature violation")
    }
    fired {
      raise wovyn event "threshold_violation"
        attributes event:attrs
    }
  }
  
  rule threshold_notification {
    select when wovyn threshold_violation
    send_directive("sms noti sent")
    always {
      raise test event "new_message"
        attributes { "to": "+"+sensor_profile:get_phone(), 
        "from": ent:send_from_phone,
        "message": "alert! temps above threshold"
        }
    }
  }
}
