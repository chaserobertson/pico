ruleset sensor_profile {
  meta {
    provides get_info,
              get_location,
              get_name,
              get_threshold,
              get_phone
    shares get_info, 
            get_location, 
            get_name, 
            get_threshold, 
            get_phone, __testing
  }
  global {
    __testing = { "queries": [ { "name": "__testing" } ],
                  "events": [ ] }
                  
    get_info = function() {
      out = {};
      out = out.put(["location"], ent:location);
      out = out.put(["name"], ent:name);
      out = out.put(["threshold"], ent:threshold);
      out = out.put(["phone"], ent:phone);
      out;
    }
    get_location = function() {
      ent:location;
    }
    get_name = function() {
      ent:name;
    }
    get_threshold = function() {
      ent:threshold;
    }
    get_phone = function() {
      ent:phone;
    }
  }
  
  rule initialization {
    select when wrangler ruleset_added where rids >< meta:rid
    if ent:location.isnull() &&
       ent:name.isnull() &&
       ent:threshold.isnull() &&
       ent:phone.isnull() then noop();
    fired {
      ent:location := "default_location";
      ent:name := "default_name";
      ent:threshold := "80";
      ent:phone := "+19499390011"
    }
  }
  
  rule profile_updated {
    select when sensor profile_updated
    pre {
      location = event:attr("location");
      name = event:attr("name");
      threshold = event:attr("threshold");
      phone = event:attr("phone");
    }
    send_directive("sensor profile updated")
    always {
      ent:location := location;
      ent:name := name;
      ent:threshold := threshold;
      ent:phone := phone;
    }
  }
}
