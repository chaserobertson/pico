angular.module('lab5prof', [])
.controller('MainCtrl', [
  '$scope','$http',
  function($scope,$http){
    $scope.location = "def";
    $scope.name = "def";
    $scope.threshold = "def";
    $scope.phone = "def";
    $scope.eci = "6hTWwHFJ2yEUEExqjXQCns";
    $scope.URL = "http://localhost:8080";

    var gURL = '/sky/cloud/'+$scope.eci+'/sensor_profile/get_info';
    $scope.getInfo = function() {
      return $http.get($scope.URL+gURL).then(function(data){
        console.log(data);
        $scope.location = data.data.location;
        $scope.name = data.data.name;
        $scope.threshold = data.data.threshold;
        $scope.phone = data.data.phone;
      });
    };

    var hURL = '/sky/event/'+$scope.eci+'/none/sensor/profile_updated';
    $scope.postInfo = function() {
      var args = "?location="+$scope.temp_loc+"&name="+$scope.temp_nam+"&threshold="+$scope.temp_thresh+"&phone="+$scope.temp_phone;
      return $http.post($scope.URL+hURL+args).then($scope.getInfo());
    };

    $scope.getInfo();
  }
]);