angular.module('lab5', [])
.controller('MainCtrl', [
  '$scope','$http',
  function($scope,$http){
    $scope.temperatures = [];
    $scope.violations = [];
    $scope.current_temp = [];
    $scope.eci = "6hTWwHFJ2yEUEExqjXQCns";
    $scope.URL = "http://localhost:8080"

    var gURL = '/sky/cloud/'+$scope.eci+'/temperature_store/current_temp';
    $scope.getCurrent = function() {
      $http.get($scope.URL+gURL).then(function(data){
        console.log(data);
        $scope.current_temp = data.data
      });
    };

    var hURL = '/sky/cloud/'+$scope.eci+'/temperature_store/temperatures';
    $scope.getTemps = function() {
      return $http.get($scope.URL+hURL).then(function(data){
        console.log(data)
        $scope.temperatures = data.data
      });
    };

    var iURL = '/sky/cloud/'+$scope.eci+'/temperature_store/threshold_violation';
    $scope.getViolations = function() {
      return $http.get($scope.URL+iURL).then(function(data){
        console.log(data)
        $scope.violations = data.data
      });
    };

    $scope.getCurrent();
    $scope.getTemps();
    $scope.getViolations();
  }
]);