ruleset temperature_store {
  meta {
    provides temperatures,
        threshold_violation,
        inrange_temperatures
    shares temperatures,
        threshold_violation, 
        inrange_temperatures, 
        __testing
  }
  global {
    __testing = { "queries": [ { "name": "__testing" } ],
                  "events": [ ] }
    temperatures = function() {
      ent:temps;
    }
    threshold_violation = function() {
      ent:violations;
    }
    inrange_temperatures = function() {
      ent:temps.filter( function(x) {
        ent:violations.indexOf(x) < 0; } )
    }
  }
  
  rule initialization {
    select when wrangler ruleset_added where rids >< meta:rid
    if ent:temps.isnull() &&
       ent:violations.isnull() then noop();
    fired {
      ent:temps := [];
      ent:violations := [];
    }
  }
  
  rule collect_temperatures {
    select when wovyn new_temperature_reading
    pre {
      new_reading = event:attrs;
    }
    send_directive("temp reading recorded")
    always {
      ent:temps := ent:temps + new_reading;
    }
  }
  
  rule collect_temperature_violations {
    select when wovyn threshold_violation
    pre {
      new_violation = event:attrs;
    }
    send_directive("threshold violation recorded")
    always {
      ent:violations := ent:violations + new_violation;
    }
  }
  
  rule clear_temperatures {
    select when sensor reading_reset
    send_directive("temperatures reset")
    always {
      ent:temps := [];
      ent:violations := [];
    }
  }
}
